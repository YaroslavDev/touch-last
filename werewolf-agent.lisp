; (run-environment (make-tl-world :max-steps 50))

(defstruct (werewolf-agent
               (:include tl-agent 
                 (body (make-werewolf-agent-body))
                 (program 'werewolf-agent-ai)
               )
          ) 
 "Your agent for  tl-world."
)

(defstruct (werewolf-agent-body 
               (:include tl-agent-body)
           )
  (prev-loc nil)  ; agent's location at previous step
  (prev-dir nil)  ; previous direction
  (prev-act nil)  ; previous action
  (prev-baba nil) ; T if on previous iteration agent was BABA
  (pyky-counter 0); agent can PYKY only with 0 pyky-counter
  (prey-dir nil)  ; stores direction of visible prey
  (prey-prev-pos nil) ; stores prey's position on previous iteration
)
;
(defun add-vectors (v1 v2)
  (mapcar #'+ v1 v2)
)

(defun scale-vector(k v)
  (mapcar #'(lambda (x) (* k x)) v)
)

(defun inverse-vector (v)
  (mapcar #'- v)
)

(defun right-vector (v)
 (list (cadr v) (- (car v)))
)

(defun left-vector (v)
 (list (- (cadr v)) (car v))
)

(defmacro case-equal (exp &body clauses)
   (let ((temp (gensym)))
    `(let ((,temp ,exp))
       (cond ,@(mapcar #'(lambda (clause)
                           (destructuring-bind (keys . clause-forms) clause
                             (cond ((eq keys 'otherwise) `(t ,@clause-forms))
                                   (t
                                    (if (atom keys) (setq keys (list keys)))
                                    `((member ,temp ',keys :test #'equal) ,@clause-forms)
                                    )
                              )
                           )
                          )
                       clauses)
       )
    )
  )
)

; Compute possible actions so that agent doesn't stuck in obstacles
(defun possible-movements (grid loc dir is-catcher)
 (let* ((back-dir (inverse-vector dir))
        (left-dir (left-vector dir))
        (right-dir (right-vector dir))
        (forward (add-vectors loc dir))
        (backward (add-vectors loc back-dir))
        (left (add-vectors loc left-dir))
        (right (add-vectors loc right-dir))
        (midleft (add-vectors loc (add-vectors left-dir dir)))
        (midright (add-vectors loc (add-vectors right-dir dir)))
        (actions nil))
   ;(format T "~&Forw: ~A Back: ~A Left: ~A Right: ~A Midleft: ~A Midright: ~A" forward backward left right midleft midright)
   ;(format t "~& AREF=~A" (aref grid (cadr forward) (car forward)))
   ;(format t "~& AREF=~A" (aref grid (cadr left) (car left)))
   ;(format t "~& AREF=~A" (aref grid (cadr right) (car right)))

   (if (eq (aref grid (cadr left) (car left)) nil) (push 'TURNLEFT actions))
   
   (if (eq (aref grid (cadr forward) (car forward)) 'WALL)
       (if (member 'TURNLEFT actions)
           (setf actions (list 'TURNLEFT))
           (setf actions (list 'TURNRIGHT))
       )
       (progn 
         (if (eq (aref grid (cadr right) (car right)) nil) (push 'TURNRIGHT actions))
         (if (eq (aref grid (cadr forward) (car forward)) nil) (push 'FORW actions))
         (if (eq (aref grid (cadr left) (car left)) nil) (push 'LEFT actions))
         (if (eq (aref grid (cadr right) (car right)) nil) (push 'RIGHT actions))
         (when is-catcher
               (progn (if (eq (aref grid (cadr midleft) (car midleft)) nil) (push 'MIDLEFT actions))
               (if (eq (aref grid (cadr midright) (car midright)) nil) (push 'MIDRIGHT actions))))
       )
   )
   ;(format t "~&ACTIONS: ~A" actions)
   actions
 )
)

; Every agent updates his individual map with obstacles
(defun update-grid (grid loc prev-loc dir prev-dir percept prev-act)
  ; 1 - If agent didn't change since last iteration, there is an obstacle
  (progn
    (if (and (member prev-act '(LEFT FORW RIGHT MIDLEFT MIDRIGHT)) (equal loc prev-loc))
        (let* ((forward (add-vectors loc dir))
               (backward (add-vectors loc (inverse-vector dir)))
               (left (add-vectors loc (left-vector dir)))
               (right (add-vectors loc (right-vector dir)))
               (midleft (add-vectors loc (add-vectors (left-vector dir) dir)))
               (midright (add-vectors loc (add-vectors (right-vector dir) dir)))
               (obstacle-loc nil)
              )
          (case prev-act
              ('LEFT (setf obstacle-loc left))
              ('FORW (setf obstacle-loc forward))
              ('RIGHT (setf obstacle-loc right))
              ('MIDLEFT (setf obstacle-loc midleft))
              ('MIDRIGHT (setf obstacle-loc midright))
              (otherwise nil)
          )
          ;(format t "~&Obstacle detected: ~A" obstacle-loc)
          (setf (aref grid (cadr obstacle-loc) (car obstacle-loc)) 'OBSTACLE)
        )
        nil
    )
    ; 2 - Update agent's grid with percept
    (do ((pos (add-vectors loc dir) (add-vectors pos dir))
        (perc percept (cdr perc)))
        ((null perc) nil)
        (if (member (car perc) '(WALL OBSTACLE)) (setf (aref grid (cadr pos) (car pos)) (car perc)))
    )
  )
)
;
(defun werewolf-agent-ai (percept)
 (let* ((agent-body (car percept))    ; extracts agent body from percept
        (is-catcher (if (tl-agent-body-ma-babu? agent-body) 'yes 'no))
        (loc (object-loc agent-body))
        (prev-loc (werewolf-agent-body-prev-loc agent-body))
        (dir (object-heading agent-body))
        (back-dir (inverse-vector dir))
        (left-dir (left-vector dir))
        (right-dir (right-vector dir))
        (prev-dir (werewolf-agent-body-prev-dir agent-body))
        (grid (tl-agent-body-grid agent-body))
        (action nil)
        (prev-act (werewolf-agent-body-prev-act agent-body))
        (pyky-counter (werewolf-agent-body-pyky-counter agent-body))
        (prev-baba (werewolf-agent-body-prev-baba agent-body))
        (prey-dir (werewolf-agent-body-prey-dir agent-body))
        (prey-prev-pos (werewolf-agent-body-prey-prev-pos agent-body))
       )
       (setf percept (cdr percept))      ; extracts proper percept
       (update-grid grid loc prev-loc dir prev-dir percept prev-act) ; updates agent's view of environment
       (format t "~&Agent : ~A" agent-body)
       (format t "~&Is-Catcher : ~A" is-catcher)
       (format t "~&Location : ~A" loc)
       (format t "~&Prey-dir: ~A" prey-dir)
       ;(format t "~&Pyky-counter: ~A" pyky-counter)
       ;(format t "~&PrevLocation : ~A" prev-loc)
       ;(format t "~&Direction : ~A" dir)
       ;(format t "~&Grid: ~&")
       ;(print-werewolf-grid grid)
       (if (tl-agent-body-ma-babu? agent-body)
            ;Werewolf
           (let ((movements (possible-movements grid loc dir T))
                 (visible-object (car (last percept)))
                 (distance-to-object (length percept))
                 )
             ;(format t "~&Distance-to-object: ~A" distance-to-object)
             ; Use pyky-counter so agents don't stuck touching themselves in infinite loop
             (if (null prev-baba) 
                 ; Now I am werewolf, but on previous iteration I wasn't -> Wait 1 iteration until I can touch
                 ; Prevent 2 agent looping: when 2 agents sees each other and repeatedly touch each other
                 (progn (setf (werewolf-agent-body-pyky-counter agent-body) 1)
                        (setf pyky-counter (werewolf-agent-body-pyky-counter agent-body))
                        (setf (werewolf-agent-body-prev-baba agent-body) T)
                 )
             )
             (if (listp visible-object)  ; only agents are represented as list e.g. (AGENT -1 0)
                 ; Werewolf sees agent
                 (if (and (= distance-to-object 1) T) ;(= pyky-counter 0)) 
                     ; Agent is close enough, touch him!
                     (progn
                       (setf action 'PYKY) 
                       (setf (werewolf-agent-body-prev-baba agent-body) nil)
                       (setf (werewolf-agent-body-prey-dir agent-body) nil)
                     )
                     ; Agent is too far from werewolf, move forward!
                     (progn 
                       (if (> distance-to-object 1) 
                           (setf action `(FORW ,(1- distance-to-object)))
                           (setf action 'FORW)
                       )
                       (setf (werewolf-agent-body-prey-dir agent-body) (list (cadr visible-object) (caddr visible-object)))
                       (setf (werewolf-agent-body-prey-prev-pos agent-body) (add-vectors loc (scale-vector distance-to-object dir)))
                     )
                 )
                 ; Werewolf doesn't see any agent
                 (if movements
                     ; Werewolf is not stuck
                     (if prey-dir
                         ; Werewolf was chasing agent at previous iteration
                       (progn
                         (if (equal left-dir prey-dir)
                             ; Agent was moving to the left, werewolf tries to go left
                            (if (member 'LEFT movements) 
                                ; Werewolf can go left, go left!
                               (setf action 'LEFT)
                               ; Werewolf can't go left, werewolf lost agent!
                               (progn
                                 (setf action (nth (random (length movements)) movements))
                               )
                            )
                            (if (equal right-dir prey-dir)
                                ; Agent was moving to the right, werewolf tries to go right
                              (if (member 'RIGHT movements)
                                  ; Werewolf can go right, go right!
                                (setf action 'RIGHT)
                                ; Werewolf can't go right, werewolf lost agent!
                                (progn
                                  (setf action (nth (random (length movements)) movements))
                                )
                              )
                               ; Agent was moving forward or backward, check if there are obstacles from left or right
                              (progn
                                ;(format t "OTHERWISE BRANCH CHOSEN!")
                                ;(format t "~& Prey-prev-pos: ~A" prey-prev-pos)
                                (let ((prey-left (add-vectors prey-prev-pos (left-vector prey-dir)))
                                      (prey-right (add-vectors prey-prev-pos (right-vector prey-dir)))
                                     )
                                  ;(format t "~& Prey-prev-left: ~A" prey-left)
                                  ;(format t "~& Prey-prev-right: ~A" prey-right)
                                  (if (member (aref grid (cadr prey-left) (car prey-left)) '(WALL OBSTACLE))
                                      ; There is obstacle on left, agent went right!
                                      (if (member 'RIGHT movements)
                                          ; Werewolf can go right, go right!
                                          (setf action 'RIGHT)
                                          ; Werewolf can't go right, pick random move
                                          (setf action (nth (random (length movements)) movements))
                                      )
                                      ; No obstacle on left, check right side
                                      (if (member (aref grid (cadr prey-right) (car prey-right)) '(WALL OBSTACLE))
                                          ; There is obstacle on right, agent went left!
                                          (if (member 'LEFT movements)
                                              ; Werewolf can go left, go left!
                                              (setf action 'LEFT)
                                              ; Werewolf can't go left, pick random move
                                              (setf action (nth (random (length movements)) movements))
                                          )
                                          ; No obstacles on left or right, pick random from 'LEFT and 'RIGHT
                                          ; Agent could go any direction
                                          (case (random 2)
                                            (0 (if (member 'RIGHT movements) 
                                                   (setf action 'RIGHT)
                                                   (if (member 'LEFT movements) (setf action 'LEFT))
                                               )
                                            )
                                            (1 (if (member 'LEFT movements) 
                                                   (setf action 'LEFT)
                                                   (if (member 'RIGHT movements) (setf action 'RIGHT))
                                               )
                                            )
                                          )
                                      )
                                  )
                                )
                                ;(setf action (nth (random (length movements)) movements))
                                
                              )
                            )
                         )
                         (setf (werewolf-agent-body-prey-dir agent-body) nil)
                       )
                      ; Werewolf wasn't chasing at previous iteration, seek for agent randomly
                       (setf action (nth (random (length movements)) movements))
                     )
                 )
             )
            )
            ;Human
            (let ((movements (possible-movements grid loc dir nil))
                  (visible-object (car (last percept)))
                  (distance-to-object (length percept)))
                ;(format t "~&Movements: ~A" movements)
              (if (and (listp visible-object) (eq (car visible-object) 'BABA))
                  ; Human sees werewolf
                  (if (member 'LEFT movements) 
                      ; Human can move left, go left!
                      (setf action 'LEFT)
                      ; Human can't move left
                      (if (member 'RIGHT movements)
                          ; Human can move right, go right!
                          (setf action 'RIGHT)
                          ; Human can't go left or right, then turnright!
                          (setf action 'TURNRIGHT)
                      )
                  )
                  ; Human doesn't see any danger, just pick random movements
                  (if movements (setf action (nth (random (length movements)) movements)))
              )
            )
       )
       (setf (werewolf-agent-body-prev-loc agent-body) loc)
       (setf (werewolf-agent-body-prev-dir agent-body) dir)
       (setf (werewolf-agent-body-prev-act agent-body) action)
       (when (> pyky-counter 0) (decf (werewolf-agent-body-pyky-counter agent-body)))
       (if (null action) (setf action 'TURNRIGHT))
       action
 )
)


(defun print-werewolf-grid (grid)
 (do ((i 9 (1- i)))
     ((< i 0) nil)
   (do ((j 0 (1+ j)))
       ((> j 9) (format t "~&"))
     (format t "~A " (aref grid i j))
   )
 )
)
